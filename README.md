# 42-doom_nukem

```
make
./doom_nukem maps/lava2
```

#### controls
```
w, a, s, d -> move
space -> jump
left click -> shoot
shift -> run
```
